<?php

namespace Rodw\TemplateParser\ClassLoader;


use Composer\Autoload\ClassLoader;

class ComposerClassLoaderAdapter implements ClassLoaderInterface
{
    /**
     * @var ClassLoader
     */
    private $classLoader;

    public function __construct(ClassLoader $classLoader)
    {
        $this->classLoader = $classLoader;
    }

    public function load($class)
    {
        $this->classLoader->loadClass($class);
    }
}