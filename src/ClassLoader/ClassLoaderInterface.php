<?php


namespace Rodw\TemplateParser\ClassLoader;


interface ClassLoaderInterface
{
    public function load($class);
}