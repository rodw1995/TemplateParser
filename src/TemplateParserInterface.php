<?php
namespace Rodw\TemplateParser;

interface TemplateParserInterface
{
    /**
     * Get the template content
     *
     * @param string $file
     * @param array $values
     * @return string
     */
    public function template($file, array $values);
}