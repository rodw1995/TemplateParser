<?php

namespace Rodw\TemplateParser;


class TemplateParserHelper
{
    /**
     * Convert string to camelCase
     *
     * @param $string
     * @return string
     */
    public static function camelCase($string)
    {
        $string = str_replace(['_', '-'], ' ', $string);

        return lcfirst(str_replace(' ', '', ucwords($string)));
    }

    /**
     * Get the short name of a fullClass name
     *
     * @param $fullClass
     * @return string
     */
    public static function toShortName($fullClass)
    {
        return basename(str_replace('\\', '/', $fullClass));
    }

    /**
     * Get the namespace of a fullClass name
     *
     * @param $fullClass
     * @return string
     */
    public static function toNamespace($fullClass)
    {
        return rtrim(str_replace(static::toShortName($fullClass), '', $fullClass), '\\');
    }

    /**
     * Parse a string with the placeholders #~ ~# with the correct values
     *
     * @param $string
     * @param array $values
     * @return string
     */
    public static function parseString($string, array $values)
    {
        return preg_replace_callback('/#~((.|\r?\n)*?)~#/', function ($matches) use ($values) {
            $match = $matches[1];

            if (isset($values[$match])) {
                return $values[$match];
            }

            // Match is not set in the values, return just the match
            return $match;

        }, $string);
    }

    /**
     * Loop through an array and prints for every element the string
     *
     * @param array $array
     * @param $string
     * @param string $betweenStrings
     * @return string
     */
    public static function loop(array $array, $string, $betweenStrings = PHP_EOL)
    {
        $output = [];

        foreach ($array as $key => $value) {
            // Flatten the array with dots and parse it
            $output[] = static::parseString($string, static::array_dot($value));
        }

        return implode($betweenStrings, $output);
    }

    /**
     * Create a new line
     *
     * @return string
     */
    public static function newLine()
    {
        return PHP_EOL;
    }

    /**
     * Create spaces
     *
     * @param int $spaces
     * @return string
     */
    public static function spaces($spaces = 4)
    {
        $string = '';

        for ($i = 0; $i < $spaces; $i++) {
            $string .= ' ';
        }

        return $string;
    }

    /**
     * Run php code and return the string that should be printed
     *
     * @param $code
     * @return string
     */
    public static function php($code)
    {
        return eval($code);
    }

    /**
     * LARAVEL 5.2 support function
     *  
     * Flatten a multi-dimensional associative array with dots.
     *
     * @param  array   $array
     * @param  string  $prepend
     * @return array
     */
    public static function array_dot($array, $prepend = '')
    {
        $results = [];

        foreach ($array as $key => $value) {
            if (is_array($value) && ! empty($value)) {
                $results = array_merge($results, static::array_dot($value, $prepend.$key.'.'));
            } else {
                $results[$prepend.$key] = $value;
            }
        }

        return $results;
    }
}