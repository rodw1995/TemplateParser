<?php

namespace Rodw\TemplateParser;


use ReflectionFunctionAbstract;
use Rodw\TemplateParser\ClassLoader\ClassLoaderInterface;

class TemplateParser implements TemplateParserInterface
{
    /**
     * @var string
     */
    private $template;

    /**
     * @var array
     */
    private $classes;

    /**
     * @var array
     */
    private $values;

    /**
     * @var ClassLoaderInterface
     */
    private $classLoader;

    public function __construct(ClassLoaderInterface $classLoader)
    {
        $this->classLoader = $classLoader;
    }

    /**
     * Get the template content
     *
     * @param string $file
     * @param array $values
     * @return string
     */
    public function template($file, array $values)
    {
        $this->values = $values;
        $this->parseTemplateFile($file);

        return $this->parse($this->template);
    }

    /**
     * Replace the placeholders in the content with the values
     *
     * @param $content
     * @return mixed
     */
    private function parse($content)
    {
        return preg_replace_callback('/{~((.|\r?\n)*?)~}/', function ($matches) {
            $match = $this->getVariable($matches[1]);

            if ($match != $matches[1]) {
                // Match has changed so the placeholder is replaced
                return $match;
            }

            return $this->parseFunctions($match);

        }, $content);
    }

    /**
     * Parse the functions that are in a {~ ~} section
     *
     * @param $match
     * @return mixed
     */
    private function parseFunctions($match)
    {
        return preg_replace_callback('/@(.*?)\((.*)\)/s', function ($functionMatches) {

            $functionName = $functionMatches[1];
            $sep = $this->getFunctionParameterSeparator();
            $parameters = preg_replace('/\s?' . preg_quote($sep) . '\s?/', $sep, $functionMatches[2]);

            // Parse all nested functions
            $startFunctionPos = true;
            while ($startFunctionPos !== false) {
                $startFunctionPos = strpos($parameters, '@');
                if ($startFunctionPos !== false) {
                    $openBracket = strpos($parameters, '(', $startFunctionPos);

                    if ($openBracket !== false) {
                        $openBracket++;
                        // Find the closingBracket
                        $openBracketCount = 1;
                        foreach (str_split(substr($parameters, $openBracket)) as $key => $char) {
                            if ($char == '(') {
                                $openBracketCount++;
                            } elseif ($char == ')') {
                                $openBracketCount--;
                                if ($openBracketCount == 0) {
                                    $closeBracket = $key + 1 + $openBracket;
                                    break;
                                }
                            }
                        }

                        if (isset($closeBracket)) {
                            $length = $closeBracket - $startFunctionPos;
                            $functionPart = substr($parameters, $startFunctionPos, $length);
                            $replacement = $this->parseFunctions($functionPart);

                            $parameters = substr_replace($parameters, $replacement, $startFunctionPos, $length);
                        }
                    }
                }
            }

            if (!empty($parameters)) {
                $parameters = explode($this->getFunctionParameterSeparator(), $parameters);
            } else {
                $parameters = [];
            }

            $parsed = $this->parseNonClassFunctions($functionName, $parameters);

            if ($parsed !== false) {
                return $parsed;
            }

            // Check imported classes
            foreach ($this->classes as $class) {
                // Try to load it
                if (!class_exists($class)) {
                    // Load class
                    $this->classLoader->load($class);

                    // Try again
                    if (!class_exists($class)) {
                        if (file_exists($class)) {
                            require_once $class;

                            $parsed = $this->parseNonClassFunctions($functionName, $parameters);

                            if ($parsed !== false) {
                                return $parsed;
                            }
                        } else {
                            // Imported class cannot be found
                            throw new \Exception('Could not import ' . $class);
                        }
                    }
                }

                $functionCall = $this->tryToCallFunction($class, $functionName, $parameters);

                if ($functionCall !== false) {
                    return $functionCall;
                }
            }

            // Try to call it on this class
            $functionCall = $this->tryToCallFunction($this, $functionName, $parameters);

            if ($functionCall !== false) {
                return $functionCall;
            }

            // Function cannot be found
            throw new \Exception('Function "' . $functionName . '" not found in any of the imported files');

        }, $match);
    }

    /**
     * Try to call a function an a class with the given parameters
     *
     * @param $class
     * @param $method
     * @param $parameters
     * @return bool|mixed
     * @throws \Exception
     */
    private function tryToCallFunction($class, $method, $parameters)
    {
        // Try to call it on this class
        $reflection = new \ReflectionClass($class);
        if ($reflection->hasMethod($method)) {
            $parameters = $this->getParameters($reflection->getMethod($method), $parameters);

            return call_user_func_array([$class, $method], $parameters);
        }

        return false;
    }

    /**
     * Get the parameters for the function
     *
     * @param ReflectionFunctionAbstract $reflection
     * @param array $parameters
     * @return array
     * @throws \Exception
     */
    private function getParameters(ReflectionFunctionAbstract $reflection, array $parameters)
    {
        $numberOfParameters = count($parameters);
        $requiredParameters = $reflection->getNumberOfRequiredParameters();
        $maximumParameters = $reflection->getNumberOfParameters();

        if ($numberOfParameters <= $maximumParameters AND $numberOfParameters >= $requiredParameters) {

            foreach ($parameters as $key => $parameter) {
                $parameters[$key] = $this->getVariable($parameter);
            }

            return $parameters;
        }

        $message = $reflection->getName() . ' has ' . $requiredParameters . ' required parameters' .
            ' with a maximum of ' . $maximumParameters . ': ' . $numberOfParameters . ' parameters given.';
        throw new \Exception($message);
    }

    /**
     * Parse a stand alone function
     *
     * @param $functionName
     * @param array $parameters
     * @return bool|mixed
     * @throws \Exception
     */
    private function parseNonClassFunctions($functionName, array $parameters)
    {
        // Check for function
        if (function_exists($functionName)) {
            $functionReflection = new \ReflectionFunction($functionName);

            $parameters = $this->getParameters($functionReflection, $parameters);

            return call_user_func_array($functionName, $parameters);
        }

        return false;
    }

    /**
     * Get a variable from the values array
     *
     * @param $variable
     * @return array
     */
    private function getVariable($variable)
    {
        $values = $this->values;

        if (isset($values[$variable])) {
            return $values[$variable];
        }

        $tempValues = $values;

        foreach (explode('.', $variable) as $key) {
            if (!isset($tempValues[$key])) {
                // No value found return the original placeholder
                return $variable;
            }

            $tempValues = $tempValues[$key];
        }

        return $tempValues;
    }

    /**
     * Parse the template file
     *  - Set the classes that are imported
     *  - Set the template
     *  - Set the declared values
     *
     * @param $file
     */
    private function parseTemplateFile($file)
    {
        $fileContent = file_get_contents($file);

        preg_match('/<declare>((.|\r?\n)*?)<\/declare>/', $fileContent, $declare);

        preg_match_all('/@import[\s]+([^\s]+)/', $declare[1], $imports);

        $classes = [];

        foreach ($imports[1] as $import) {
            $classes[] = $import;
        }

        $this->classes = $classes;

        preg_match_all('/@var[\s]+(.+?)(?=~;)/s', $declare[1], $values);

        foreach ($values[1] as $value) {
            $assignPos = strpos($value, '=');
            if ($assignPos !== false) {
                $key = rtrim(substr($value, 0, $assignPos));
                $value = ltrim(substr($value, $assignPos + 1));

                // Parse the value, so it will be possible to use functions and variables
                $this->values[$key] = $this->parse($value);
            }
        }

        $templateStartPos = strpos($fileContent, '</declare>');

        if ($templateStartPos !== false) {
            $lineBreaks = [PHP_EOL, "\n", "\r\n"];

            $newTemplateStartPos = false;
            foreach ($lineBreaks as $lineBreak) {
                $newTemplateStartPos = strpos($fileContent, $lineBreak, $templateStartPos);
                if ($newTemplateStartPos !== false) {
                    $templateStartPos = $newTemplateStartPos + strlen($lineBreak);
                    break;
                }
            }

            // Check if the new start position is found
            if ($newTemplateStartPos === false) {
                // Set the start position after the declare
                $templateStartPos += strlen('</declare>');
            } 
        } else {
            // No declare section in the file, the whole file is the template
            $templateStartPos = 0;
        }

        $template = substr($fileContent, $templateStartPos);

        $this->template = $template;
    }

    private function getFunctionParameterSeparator()
    {
        if (isset($this->values['functionParameterSeparator'])) {
            return $this->values['functionParameterSeparator'];
        }

        return ',';
    }
}